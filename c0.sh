#!/bin/bash
#compile and link
echo "Script File c0.sh"
yasm -g dwarf2 -iformat=yasm -f elf64 -o asm0.o asm0.asm -l asm0.lst
gcc -c -S -masm=intel casm0.c
gcc -c -Wa,-adhln -g -masm=intel casm0.c > casm0.lst
gcc -Xlinker -Map=casm0.map -o casm0 casm0.o asm0.o
echo "Done"
