//-----------------------------------------------
//Dr. Karne 
//Lab0 to illustrate some functions of ASM code 
//calab0.c
//asm0.asm
//-----------------------------------------------
#include <stdio.h> 
#include <stdlib.h> 

//C prototype 
long addnumbers(long v1, long v2, long v3, 
		long v4, long v5, long v6);

long subnumbers(long v1, long v2, long v3,
		long v4, long v5, long v6);

long testasm(long v1, long v2); //function, value 

//six parameters by value 
//they will be passed to asm call through registers 
//  rdi, rsi, rdx, rcx, r8, r9

int main(int argc, char **argv) 
{
    long val1=0, val2=0, val3=0, val4=0,val5=0, val6=0; 
    long result=0; 
    int i=0; 
    char *cptr; 

    cptr = (char *)malloc (50); //50 bytes  
    for (i=0; i<50; i++) 
      cptr[i] = 0x30+i; 
      

    val1  = 0x0199999999999999; 
    val2  = 0x0288888888888888; 
    val3  = 0x0388888888888888; 
    val4  = 0x0488888888888888; 
    val5  = 0x0599999999999999; 
    val6  = 0x0699999999999999; 
	printf ("Val1=%lx\n", val1);
	printf ("Val2=%lx\n", val2);
	printf ("Val3=%lx\n", val3);
	printf ("Val4=%lx\n", val4);
	printf ("Val5=%lx\n", val5);
	printf ("Val6=%lx\n", val6);
    printf ("This is adding 8 byte numbers program\n"); 

    result = addnumbers(val1, val2, val3, val4, val5, val6); 
    printf("Result after addition: %lx\n\n", result); 
    result = subnumbers(val1, val2, val3, val4, val5, val6);
    printf("Result after subtraction: %lx\n\n", result); 
    //------------------------------------------------------
    //using testasm, many programming constructs can be tested
    //------------------------------------------------------
    //for loop  
    result = testasm(1, 100); //1:for loop function 100 times 
    printf("Result:for loop 100 times:    %lx\n", result); 
    //while loop 
    result = testasm(2, 100); //2:while loop function 100 times 
    printf("Result:while loop 100 times:  %lx\n", result); 
    //switch 
    result = testasm(3, 2); //3:switch function, case no 1-5  
    if (result > 0) 
     printf("Result:case selected:         %lx\n", result); 
    else 
     printf("Result:wrong case selected:   %lx\n", result); 


    return 0; 
}
